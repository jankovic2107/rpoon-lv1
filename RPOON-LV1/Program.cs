﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note();
            Console.WriteLine("Note: " + note1.getRecordInNote() + "\tAuthor of this note: " + note1.getNoteAuthor());

            Note note2 = new Note("Snow in march!", 5);
            Console.WriteLine("Note: " + note2.getRecordInNote() + "\tAuthor of this note: " + note2.getNoteAuthor());

            Note note3 = new Note("Corona virus", "David", 3);
            Console.WriteLine("Note: " + note3.getRecordInNote() + "\tAuthor of this note: " + note3.getNoteAuthor() + "\n\n");

            note1.RecordInNote = "Danas je utorak";
            Console.WriteLine(note1.RecordInNote + "\n\n");

            Console.WriteLine(note1.ToString());
            Console.WriteLine(note2.ToString());
            Console.WriteLine(note3.ToString());

            Console.WriteLine("\n\n");
            NoteWithTime timeNote = new NoteWithTime("Vani je snijeg", "Marko", 1, DateTime.Now);
            Console.WriteLine(timeNote.ToString());
            Console.WriteLine("\n\n");

        }
    }
}
