﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV1
{
    class NoteWithTime : Note
    {
        private DateTime time;
        public NoteWithTime() : base()
        {
            this.time = DateTime.Now;
        }
        public NoteWithTime(string Record, string Author, int Significance, DateTime time):base(Record, Author, Significance)
        {
            this.time = time;
        }
        public DateTime Time
        {
            get { return time; }
            set { this.time = value; }
        }
        public override string ToString()
        {
            return base.ToString() + "\tTime: " + time;
        }
    }
}
