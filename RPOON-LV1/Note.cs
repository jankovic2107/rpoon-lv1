﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV1
{
    class Note
    {
        private string recordInNote;
        private string noteAuthor;
        private int significanceOfNote;

        public string getRecordInNote() { return recordInNote; }
        public string getNoteAuthor() { return noteAuthor; }
        public int getSignificationOfNote() { return significanceOfNote; }

        public void setRecordInNote(string Record) { this.recordInNote = Record; }
        public void setSignificanceOfNote(int Significance) { this.significanceOfNote = Significance; }

        public string RecordInNote
        {
            get { return recordInNote; }
            set { this.recordInNote = value; }
        }
        public string NoteAuthor
        {
            get { return noteAuthor; }
        }
        public int SignificanceOfNote
        {
            get { return significanceOfNote; }
            set { this.significanceOfNote = value; }
        }

        public Note()
        {
            this.recordInNote = "There is no given note!!";
            this.noteAuthor = "Uknown author";
            this.significanceOfNote = 0;
        }
        public Note(string Record, int Significance)
        {
            this.recordInNote = Record;
            this.noteAuthor = "Unknown author";
            this.significanceOfNote = Significance;
        }
        public Note(string Record, string Author, int Significance)
        {
            this.recordInNote = Record;
            this.noteAuthor = Author;
            this.significanceOfNote = Significance;
        }

        public override string ToString()
        {
            return "Note: " + recordInNote + "\tAuthor: " + noteAuthor;
        }
    }
}
